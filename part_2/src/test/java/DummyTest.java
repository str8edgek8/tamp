import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

@Slf4j
@Data
@NoArgsConstructor
public class DummyTest {

    private double firstOperand;
    private double secondOperand;

    public DummyTest(double firstOperand, double secondOperand) {
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
    }

    @Test(groups = "include-test-one")
    @Parameters({"firstOperand", "secondOperand"})
    public void dummyTestParameters(int firstOperand, int secondOperand) {
        log.info("Operation : summary");
        log.info("First operand: " + firstOperand);
        log.info("Second operand: " + secondOperand);
        log.info("Result: " + (firstOperand + secondOperand));
    }

    @Test(groups = "include-test-one", dataProvider = "dp")
    public void dummyTestDataProvider(int firstOperand, int secondOperand) {
        log.info("Operation : multiplication");
        log.info("First operand: " + firstOperand);
        log.info("Second operand: " + secondOperand);
        log.info("Result: " + (firstOperand * secondOperand));
    }

    @DataProvider(name = "dp")
    public Object[][] dpMethod() {
        return new Object[][]{{15, 25}, {-20, 36}};
    }

    @Test(groups = "include-test-one")
    public void dummyTestFactory() {
        log.info("Operation : division");
        log.info("First operand: " + firstOperand);
        log.info("Second operand: " + secondOperand);
        log.info("Result: " + (firstOperand / secondOperand));
    }

    @Factory()
    public Object[] factoryMethod() {
        return new Object[]{new DummyTest(10.5, 0.5), new DummyTest(1.5, 3.25)};
    }
}
