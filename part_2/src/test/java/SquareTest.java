import org.testng.Assert;
import org.testng.annotations.Test;

public class SquareTest {

    @Test
    public void square() {
        Rectangle re = new Square();
        re.setWidth(5);
        re.setHeight(6);
        Assert.assertEquals(re.area(), 30.0);
        //lsp - square doesn't replace rectangle
    }
}
