public class Service {

    //S - single responsibility

    public void createStudent(Student student){
        //do something
    }

    public void createCourse(Course course){
        //do something
    }
}
