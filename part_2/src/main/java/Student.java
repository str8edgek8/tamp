public class Student implements  Study{
    private String type;

    //o - open closed

    public void introduce() {
        if ("highSchool".equals(type)) {
            System.out.println("I am an high school student");
        } else if ("underGraduate".equals(type)) {
            System.out.println("I am a under graduate student");
        } else {
            System.out.println("I am a common student");
        }
    }

    @Override
    public void createThesis() {
        //I - Student doesn't heed thesis
    }

    @Override
    public void createPaper() {

    }
}
